/**
 *@author Brooks Johnson 
 *@date 7/13/2020
 * @description Demo class for version control
 */

public with sharing class HelloGit {

    public static void sayHelloGit(){
        System.debug('Git is soooo cool');
    }

    public Boolean isGitCool(){
        return true;
    }
}